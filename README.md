# To UK

## VISA application

General Visit Visa (6 months or less)
Most applications are processed within **15 days**.

[check Visa processing time](https://visa-processingtimes.homeoffice.gov.uk/y/dublin-irish-republic/visits-visas/general-visit-6-months-or-less)

### Fees

Short-term (up to 6 months, single or multiple entry) **105 eur**

[check Visa fee](https://visa-fees.homeoffice.gov.uk/y/ireland/eur/visit/standard-visitor/all)


### Document list

When you apply you’ll need to provide:

- a current **passport** or other valid travel identification
- evidence that you can support yourself during your trip, such as **bank statements or payslips** from the last **6** months
- a letter from your employer on company headed paper, detailing your role, salary and length of employment
- or a letter from your education provider, on headed paper, confirming your enrolment and leave of absence

[full online list](https://www.gov.uk/government/publications/visitor-visa-guide-to-supporting-documents)

You’ll need to provide the following:

- the dates you’re planning to travel to the UK
- details of where you’ll be staying during your visit
- how much you think your trip will cost
- your current home address and how long you’ve lived there
- your parents’ names and dates of birth
- how much you earn in a year

You might also need:

- details of your travel history for the past 10 years (as shown in your passport)
- your employer’s address and telephone number
- details of any criminal, civil or immigration offences you have committed


### Appointment checklist

* [ ] passport
* [ ] payslips
* [ ] bank statements
